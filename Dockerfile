FROM ubuntu:14.04

# Install dependencies
RUN  apt update && \
     apt install -yq \
        vim \
        wget \
        openssh-server \
        gfortran \
        gcc \
        make \
        automake \
        m4 \
        libtool \
        libgsl0-dev \
        libblas-dev \
        liblas-dev \
        liblapack-dev \
        libfftw3-dev  \
        curl && \
    apt-get install openmpi-bin libopenmpi-dev --assume-yes && \
    mkdir -p /octo/ && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Change working directory
WORKDIR /octo

# Install Octopus
RUN mkdir bin && \
    curl -JLO tddft.org/programs/octopus/down.php?file=4.1.2/octopus-4.1.2.tar.gz && \
    curl -JLO http://www.tddft.org/programs/octopus/down.php?file=libxc/libxc-2.1.2.tar.gz && \
    curl -JLO http://ftpmirror.gnu.org/gsl/gsl-1.14.tar.gz && \
    tar xvzf octopus-4.1.2.tar.gz && \
    tar xvzf libxc-2.1.2.tar.gz && \
    tar xvzf gsl-1.14.tar.gz && \
    rm octopus-4.1.2.tar.gz libxc-2.1.2.tar.gz gsl-1.14.tar.gz && \
    cd ./libxc-2.1.2 && ./configure --prefix=/octo/bin FC=gfortran F77=gfortran CC=gcc && make && make install && \
    cd ../gsl-1.14 && ./configure --prefix=/octo/bin && make && make install && \
    cd /octo/octopus-4.1.2 && ./configure CC=mpicc FC=mpif90 --with-libxc-prefix=/octo/bin --enable-mpi && make && make install && \
    cd && echo "PATH=/octo/bin:${PATH}" >>.bashrc
